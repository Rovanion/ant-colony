package antColony;

public class Settings {

	//BOARD
	public static final int FACTOR = 5;
	public static int ANTS = 100;
	
	public static final int I = 1; 
	
	//ANT
	public static int FERMONEAMOUNT = 5000*I;
	public static int PROBABILITY_SOFTNER = 10*I;
	
	//TILE
	public static int MAX_FEREMONE = 1000000*I;
	public static int MIN_FEREMONE = 00;
	public static int DECREASE_RATE = 1;
	
}
