package antColony;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

public class Ant implements Renderable, Updateable {
	private Tile currentTile;
	private Queue<Tile> lastTiles = new ArrayBlockingQueue<Tile>(10);
	private boolean lookingForFood;
	private int stepsFromHomeOrFood = 0;
	private Random rand = new Random();
	
	
	public Ant(Tile currentTile) {
		this.currentTile = currentTile;
		lookingForFood = true;
	}
	
	@Override
	public void render(Graphics g, int factor) {
		if (lookingForFood) {
			g.setColor(Color.BLACK);
		} else {
			g.setColor(Color.CYAN);
		}		
		g.fillRect(currentTile.getX()*factor, currentTile.getY()*factor, factor, factor);	
	}

	@Override
	public void update() {
		stepsFromHomeOrFood++;
		if(currentTile.isFood()) {
			lookingForFood = false;
			stepsFromHomeOrFood = 0;
			lastTiles.clear();
		} else if (currentTile.isNest()) {
			stepsFromHomeOrFood = 0;
			if (!lookingForFood) {
				Board.COLLECTED_FOOD++;
			}
			lookingForFood = true;
			lastTiles.clear();
		}
		
		move();
	}

	private void move() {
		dropFermone();
		
		// Set 50 as the probabilitySoftener
		currentTile = randomNeighbour(Settings.PROBABILITY_SOFTNER);
	}
	
	
	private void dropFermone() {
		int amount = Settings.FERMONEAMOUNT - stepsFromHomeOrFood;
		if(lookingForFood)
			currentTile.addBlueFermone(amount);
		else
			currentTile.addRedFermone(amount);
	}
	
	
	/**
	 * This method picks some random float. Then it compares that random float to see which neighbour it "hit"
	 * Say neighbour A is 0.0-0.5 and B is 0.51-1.0
	 * @return
	 */
	private Tile randomNeighbour(int probabilitySoftener) {
		ArrayList<Tile> neighbours = currentTile.getNeighbours();
		ArrayList<Tile> probablity = new ArrayList<Tile>();
		float totalFermone = 0;
		
		// Shuffle the list of neighbours to provide some more randomness
		Collections.shuffle(neighbours, new Random(System.nanoTime()));
		
		// Calculate the total sum of adjacent blue/red fermone
		for (Tile tile : neighbours) {
			if(lookingForFood) {
				totalFermone += tile.getRedFermone();
			} else {
				totalFermone += tile.getBlueFermone();
			}
			
			// Add the probabilitySoftener for each Tile
			totalFermone += probabilitySoftener;
		}
		
		// totalFermone == 0? Return a random neighbour.
		if(totalFermone == 0) {
			Tile t = neighbours.get(rand.nextInt(neighbours.size()));
			// We don't go back to the same tile we were just at.
			while(lastTiles.contains(t)){
				t = neighbours.get(rand.nextInt(neighbours.size()));
			}
			lastTiles.add(currentTile);
			return t;
		}
				
		float randomOutcome = rand.nextFloat();
		float probabilityInterval;
		float previousSum = 0.0f;
		
		// Check to see which one "got hit"
		for(int i = 0; i < neighbours.size(); i++) {
			
			// Add the probabilitySoftener to the numerator in part/whole
			float numerator = probabilitySoftener;
			
			if(lookingForFood) {
				numerator += neighbours.get(i).getRedFermone();
			} else {
				numerator += neighbours.get(i).getBlueFermone();
			}
			
			// Calculate the part of 1.0 for each neighbour
			probabilityInterval = numerator / totalFermone;
									
			// Check if the random float was within this neighbours "interval"
			if(randomOutcome >= previousSum && randomOutcome < probabilityInterval && !lastTiles.contains(neighbours.get(i))) {
				return neighbours.get(i);
			}
			
			// Add this to create intervals
			previousSum += probabilityInterval;
		}
		
		// Returns currentTile as some sort of ensurance. Instead of null
		return neighbours.get(neighbours.size() - 1);
	}	
}
