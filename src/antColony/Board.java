package antColony;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JFrame;

public class Board extends JFrame {
	
	public static int COLLECTED_FOOD = 000;
	
	private Tile[][] tiles;
	
	private ArrayList<Renderable> renderObjects;
	private ArrayList<Updateable> updateObjects;
	
	private Tile nestTile;
	private Tile foodTile;
	
	
	public Board(int width, int height) {
		
		COLLECTED_FOOD = 0;
		
		tiles = new Tile[width][height];
		
		renderObjects = new ArrayList<Renderable>();
		updateObjects = new ArrayList<Updateable>();
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Tile t = new Tile(i,j);
				
				renderObjects.add(t);
				updateObjects.add(t);
				
				tiles[i][j] = t;
				
				if (i != 0) {
					t.addNeighbour(tiles[i-1][j]);
					tiles[i-1][j].addNeighbour(t);
				}
				if (j != 0) {
					t.addNeighbour(tiles[i][j-1]);
					tiles[i][j-1].addNeighbour(t);
				}
				if (i != 0 && j != 0) {
					t.addNeighbour(tiles[i-1][j-1]);
					tiles[i-1][j-1].addNeighbour(t);
				}
			}
		}
		
		
		
		foodTile = tiles[10][10];
		foodTile.setType(Tile.FOOD_TYPE);
		
		nestTile = tiles[width-10][height-10];
		nestTile.setType(Tile.NEST_TYPE);
		
		for (int i = 0; i < Settings.ANTS; i++) {
			Ant a = new Ant(nestTile);
			updateObjects.add(a);
			renderObjects.add(a);
		}
		
		setVisible(true);
		setPreferredSize(new Dimension(width*Settings.FACTOR, height*Settings.FACTOR));
		setLocation(100, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		
		int i = 0;
		
		while(/*i++ < 1E5*/true) {
			
			
			
			this.update();
			this.repaint();
			
//			try {
//				Thread.sleep(1);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}			
		}
		
//		System.out.println("Food: "+Board.COLLECTED_FOOD);
//		System.out.println("Ants:"+Settings.ANTS);
//		System.out.println("Decrese rate: "+Settings.DECREASE_RATE);
//		System.out.println("Fermone Amount: "+Settings.FERMONEAMOUNT);
//		System.out.println("Max: "+Settings.MAX_FEREMONE);
//		System.out.println("Min: "+Settings.MIN_FEREMONE);
//		System.out.println("Prob Softner: "+Settings.PROBABILITY_SOFTNER);
//		System.out.println();
		
		
	}
	
	public int[] getRes() {
		return new int[]{Board.COLLECTED_FOOD,
				Settings.ANTS,
				Settings.DECREASE_RATE,
				Settings.FERMONEAMOUNT,
				Settings.MAX_FEREMONE,
				Settings.MIN_FEREMONE,
				Settings.PROBABILITY_SOFTNER};
	}
	
	public void update() {
		for (Updateable updateObject : updateObjects) {
			updateObject.update();
		}
	}
	
	
	@Override
	public void paint(Graphics g) {
		for(int i = 0; i < renderObjects.size(); i++) {
			renderObjects.get(i).render(g, Settings.FACTOR);
		}
//		g.setColor(Color.CYAN);
//		g.fillRect(0, 20, 50, 20);
//		g.setColor(Color.BLACK);
//		g.drawString(COLLECTED_FOOD+"", 5, 35);
	}
	
	
	public static void main(String[] args) {
		
//		int[] res = null;
//		
//		for (int i = 10; i <= 20; i++) {		//20*3*10*10*11*10
//			for (int j = 1; j <= 3; j++) {
//				for (int j2 = 5; j2 <= 10; j2++) {
//					for (int k = 5; k <= 10; k++) {
//						for (int k2 = 0; k2 <= 10; k2++) {
//							for (int l = 5; l <= 10; l++) {
//								
//								Settings.ANTS = 200*i;
//								Settings.DECREASE_RATE = j;
//								Settings.FERMONEAMOUNT = 10*j2;
//								Settings.MAX_FEREMONE = 100*k;
//								Settings.MIN_FEREMONE = 100*k2;
//								Settings.PROBABILITY_SOFTNER = 100*l;
//								
//								try {
//									
									Board b = new Board(100, 100);
//									int[] newRes = b.getRes();
//									for (int i : newRes) {
//										System.out.println(i);
//									}
									
									
//									if (res == null) {
//										res = newRes;
//									} else if (newRes[0] > res[0]) {
//										res = newRes;
//									}
//									
//									
//								} catch (Exception e) {
//									System.out.println("Fel! Fel! Fel!");
//								}
//								
//								
//							}
//						}
//					}
//				}
//			}
//		}
		
//		for (int i : res) {
//			System.out.println(i);
//		}
		
		
	}

	
}
