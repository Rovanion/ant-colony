package antColony;

import java.awt.Graphics;

public interface Renderable {
	public void render(Graphics g, int factor); 
}
