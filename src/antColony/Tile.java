package antColony;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashSet;

public class Tile implements Renderable, Updateable {

	public static final int REGULAR_TYPE = 0;
	public static final int NEST_TYPE = 1;
	public static final int FOOD_TYPE = 2;
	public static final int WALL_TYPE = 3;
	
	private int x;
	private int y;
	private ArrayList<Tile> neighbours;
	private int type;
	
	private int blueFermone;
	private int redFermone;
	private Color blue;
	private Color red;
	
	
	public Tile(int x, int y, int type) {
		
		this.type = type;
		
		blueFermone = Settings.MIN_FEREMONE;
		redFermone = Settings.MIN_FEREMONE;
		
		neighbours = new ArrayList<Tile>();
		
		this.x = x;
		this.y = y;
		
		blue = new Color(0,0,255);
		red = new Color(255,0,0);
	}
	
	public Tile(int x, int y) {
		this(x, y, REGULAR_TYPE);
	}
	
	
	@Override
	public void render(Graphics g, int factor) {
		switch (type) {
			case REGULAR_TYPE:
				
				Color blendedBackground = Color.WHITE;				
				
				if(getBlueFermone() > 0 || getRedFermone() > 0) {
					float quote = getBlueFermone() / (float)(Settings.FERMONEAMOUNT * 30);
					float s = (quote > 1.0) ? 1.0f : quote;
					Color blue = Color.getHSBColor(0.65f, s, 1.0f);
					
					quote = getRedFermone() / (float)(Settings.FERMONEAMOUNT * 20);
					s = (quote > 1.0) ? 1.0f : quote;
					Color red = Color.getHSBColor(0.0f, s, 1.0f);
					
					blendedBackground = ColorHelper.blendColors(blue, red, 0.5f);
				}
				
				g.setColor(blendedBackground);
				g.fillRect(x*factor, y*factor, factor, factor);
								
				break;
			
			case NEST_TYPE:
				g.setColor(Color.YELLOW);
				g.fillRect(x*factor, y*factor, factor, factor);
				break;
			
			case FOOD_TYPE:
				g.setColor(Color.GREEN);
				g.fillRect(x*factor, y*factor, factor, factor);
				break;
			
			default:
				break;
		}	
	}

	@Override
	public void update() {
		blueFermone = (blueFermone > Settings.MIN_FEREMONE) ? blueFermone - Settings.DECREASE_RATE : 0;
		redFermone = (redFermone > Settings.MIN_FEREMONE) ? redFermone - Settings.DECREASE_RATE : 0;
	}
	
	public void addBlueFermone(int amount) {
		if (blueFermone < Settings.MAX_FEREMONE) {
			this.blueFermone += amount;
		}
	}
	
	public void addRedFermone(int amount) {
		if (redFermone < Settings.MAX_FEREMONE) {
			this.redFermone += amount;
		}
	}
	
	public void addNeighbour(Tile neighbour){
		neighbours.add(neighbour);
	}
	
	public void setType(int type) {
		 this.type = type;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getBlueFermone() {
		return blueFermone;
	}

	public int getRedFermone() {
		return redFermone;
	}
	
	public ArrayList<Tile> getNeighbours() {
		return neighbours;
	}

	public boolean isFood() {
		return type == FOOD_TYPE; 
	}
	
	public boolean isNest() {
		return type == NEST_TYPE;
	}	
}
