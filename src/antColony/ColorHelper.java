package antColony;

import java.awt.Color;
import java.util.ArrayList;

public class ColorHelper {

	/**
	 * Blend two colors with given ratio.
	 */
	public static Color blendColors(Color color1, Color color2, float ratio) {

		float rgb1[] = new float[3];
		float rgb2[] = new float[3];

		color1.getColorComponents(rgb1);
		color2.getColorComponents(rgb2);

		Color color = new Color(
			rgb1[0] * ratio + rgb2[0] * (1-ratio),
			rgb1[1] * ratio + rgb2[1] * (1-ratio),
			rgb1[2] * ratio + rgb2[2] * (1-ratio)
		);

		return color;
	}
}
